import java.util.*;

public class Main {

	private static int w, maxW;
	private static int h, maxH;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		w = in.nextInt();
		h = in.nextInt();
		int n = in.nextInt();
		TreeMap<Integer, Integer> vertical = new TreeMap<>(), horizontal = new TreeMap<>();
		vertical.put(0, h);
		horizontal.put(0, w);
		maxW = 0;
		maxH = 0;
		for (int i = 0; i < n; i++) {
			char side = in.next().charAt(0);
			int val = in.nextInt();

			if (side == 'V') {
				maxW = insert(val, maxW, horizontal);
			} else {
				maxH = insert(val, maxH, vertical);
			}

			//System.out.println(vertical.get(maxH) * horizontal.get(maxW));
			System.out.println(Collections.max(vertical.values())*Collections.max(horizontal.values()));
		}

	}

	public static int insert(int pos, int maxIndex, TreeMap<Integer, Integer> target) {
		Map.Entry<Integer, Integer> insertPos = target.floorEntry(pos);
		int tmp = pos - insertPos.getKey();
		if (insertPos.getKey() == maxIndex) {
			maxIndex = tmp >= insertPos.getValue() - tmp ? maxIndex : pos;
		}

		target.put(pos, insertPos.getValue() - tmp);
		target.put(insertPos.getKey(), tmp);

		return maxIndex;
	}
}